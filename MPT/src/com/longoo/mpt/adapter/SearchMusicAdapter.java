package com.longoo.mpt.adapter;

import java.util.List;

import com.longoo.mpt.R;
import com.longoo.mpt.networkres.MusicDataResult;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SearchMusicAdapter extends BaseAdapter {
	private static final String TAG = "XmlMusicResLoader";

	private Context mContext;
	private LayoutInflater mInflater;
	private List<MusicDataResult> mSongs;
	
	public SearchMusicAdapter(final Context context) {
		this.mInflater = LayoutInflater.from(context);
		this.mContext = context;
	}

	public List<MusicDataResult> getSongs() {
		return this.mSongs;
	}

	public void setSongs(List<MusicDataResult> songs) {
		this.mSongs = songs;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return this.mSongs != null ? this.mSongs.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		convertView = bindView(position, convertView, parent);
		return convertView;
	}

	public View bindView(final int position, View convertView, final ViewGroup parent) {
		MyViewHolder holder = null;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_search_music_info, parent, false);
			holder = new MyViewHolder(convertView);
			convertView.setTag(holder);
		} else {
			holder = (MyViewHolder) convertView.getTag();
		}
		//
		MusicDataResult songInfo = mSongs.get(position);
		if (songInfo != null) {
			holder.song_name_tv.setText("" + songInfo.getSong_name());
			holder.singer_name_tv.setText("" + songInfo.getSinger_name());
		}
		return convertView;
	}

	public class MyViewHolder {
		public TextView song_name_tv;
		public TextView singer_name_tv;
		
		public MyViewHolder(View convertView) {
			song_name_tv = (TextView) convertView.findViewById(R.id.song_name_tv);
			singer_name_tv = (TextView) convertView.findViewById(R.id.singer_name_tv);
		}
	}

}
