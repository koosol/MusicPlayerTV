package com.longoo.mpt.activity;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.xutils.x;
import org.xutils.common.Callback.Cancelable;
import org.xutils.common.Callback.CommonCallback;
import org.xutils.http.RequestParams;

import com.google.gson.Gson;
import com.longoo.mpt.R;
import com.longoo.mpt.adapter.SearchMusicAdapter;
import com.longoo.mpt.musiclrc.DefaultLrcParser;
import com.longoo.mpt.musiclrc.LrcRow;
import com.longoo.mpt.musiclrc.LrcView;
import com.longoo.mpt.networkres.MusicCommonCallback;
import com.longoo.mpt.networkres.MusicDataResult;
import com.longoo.mpt.networkres.MusicInfoResultAbs;
import com.longoo.mpt.networkres.MusicNetWorkRes;
import com.longoo.mpt.networkres.MusicSongInfo;
import com.longoo.mpt.networkres.OnlineLrcResult;
import com.longoo.mpt.networkres.OnlineLrcResult.LrcInfo;
import com.longoo.mpt.networkres.XmlMusicResLoader;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

public class MainActivity extends Activity implements OnClickListener {

	private static final String TAG = "XmlMusicResLoader";

	EditText search_et;
	ImageView pic_iv;
	XmlMusicResLoader xmlMusicResLoader;
	HashMap<String, MusicNetWorkRes> mMusicSearchMap;
	HashMap<String, MusicNetWorkRes> mMusicSongInfoMap;
	ListView music_listview;
	LrcView lrcView;
 	SearchMusicAdapter mSearchMusicAdapter;
	Context mContext;
	private MediaPlayer mp = new MediaPlayer();
	private String type = "kuwo";
	private MusicInfoResultAbs mMusicInfoResultAbs = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		x.Ext.init(getApplication());
		setContentView(R.layout.activity_main);
		mContext = getApplicationContext();
		xmlMusicResLoader = new XmlMusicResLoader(getApplicationContext());
		mMusicSearchMap = xmlMusicResLoader.getMusicSearchMap();
		mMusicSongInfoMap = xmlMusicResLoader.getMusicSongInfoMap();
		//
		lrcView = (LrcView) findViewById(R.id.lrcView);
		search_et = (EditText) findViewById(R.id.search_et);
		pic_iv = (ImageView) findViewById(R.id.pic_iv);
		//
		Button button1 = (Button) findViewById(R.id.button1);
		Button button2 = (Button) findViewById(R.id.button2);
		Button button3 = (Button) findViewById(R.id.button3);
		Button button4 = (Button) findViewById(R.id.button4);
		Button button5 = (Button) findViewById(R.id.button5);
		Button button6 = (Button) findViewById(R.id.button6);
		Button button13 = (Button) findViewById(R.id.button13);
		//
		Button button7 = (Button) findViewById(R.id.button7);
		Button button8 = (Button) findViewById(R.id.button8);
		Button button9 = (Button) findViewById(R.id.button9);
		Button button10 = (Button) findViewById(R.id.button10);
		Button button11 = (Button) findViewById(R.id.button11);
		Button button12 = (Button) findViewById(R.id.button12);
		Button button14 = (Button) findViewById(R.id.button14);
		//
		Button button15 = (Button) findViewById(R.id.button15);
		Button button16 = (Button) findViewById(R.id.button16);
		Button button17 = (Button) findViewById(R.id.button17);
		Button button18 = (Button) findViewById(R.id.button18);
		Button test_btn = (Button) findViewById(R.id.test_btn);
		//
		music_listview = (ListView) findViewById(R.id.music_listview);
		mSearchMusicAdapter = new SearchMusicAdapter(mContext);
		music_listview.setAdapter(mSearchMusicAdapter);
		music_listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
				List<MusicDataResult> songInfos = mSearchMusicAdapter.getSongs();
				MusicDataResult songInfo = songInfos.get(position);
				Log.d(TAG, " onItemClick songInfo:" + songInfo);
				xmlMusicResLoader.maicong_get_song_by_id("" + songInfo.getRadio_songid(), type,
						new MusicCommonCallback() {
					@Override
					public void onError(Throwable throwa, boolean isError) {
						Log.d(TAG, "获取歌曲真实失败:" + throwa.getMessage());
					}

					@Override
					public void onSuccess(final MusicSongInfo songInfo) {
						if (songInfo != null) {
							final String playAddr = songInfo.getMusic();
							if (!TextUtils.isEmpty(playAddr)) {
								x.image().bind(pic_iv, songInfo.getPic());
								 playNetWorkAddr(playAddr);
								 xmlMusicResLoader.getOnlineLrc(songInfo.getName(), songInfo.getAuthor(), new MusicCommonCallback() {
									 @Override
									public void onError(Throwable ex, boolean isOnCallback) {
										 Log.d(TAG, "获取歌词失败!!" + ex);
									}
									 @Override
									public void onFinished() {
										 Log.d(TAG, "获取歌词完成!!");
									}
									 public void onSuccess(String res) {
										 if (TextUtils.isEmpty(res))
											 return;
										 Gson gson = new Gson();
										 OnlineLrcResult onlineLrcResult = gson.fromJson(res, OnlineLrcResult.class);
										 List<LrcInfo> lrcInfos = onlineLrcResult.getResult();
										 Log.d(TAG, "获取歌词完成!!" + lrcInfos);
										if (lrcInfos != null && lrcInfos.size() > 0) {
											LrcInfo lrInfo = lrcInfos.get(0);
											String lrcAddr = lrInfo.getLrc();
											RequestParams params = new RequestParams(lrcAddr);
											x.http().get(params, new CommonCallback<String>() {
												@Override
												public void onCancelled(CancelledException arg0) {
												}
												@Override
												public void onError(Throwable arg0, boolean arg1) {
												}
												@Override
												public void onFinished() {
												}
												@Override
												public void onSuccess(String res) {
													// 获取歌词.
													lrcView.setLrcRows(DefaultLrcParser.getIstance().getLrcRows(res));
												}
											});
										}
									 };
								 });
							}
						}
					}
				});
			}
		});
		//
		button1.setOnClickListener(this);
		button2.setOnClickListener(this);
		button3.setOnClickListener(this);
		button4.setOnClickListener(this);
		button5.setOnClickListener(this);
		button6.setOnClickListener(this);
		button7.setOnClickListener(this);
		button8.setOnClickListener(this);
		button9.setOnClickListener(this);
		button11.setOnClickListener(this);
		button12.setOnClickListener(this);
		button13.setOnClickListener(this);
		button14.setOnClickListener(this);
		button15.setOnClickListener(this);
		button16.setOnClickListener(this);
		button17.setOnClickListener(this);
		//
		button18.setOnClickListener(this);
		test_btn.setOnClickListener(this);
		//
		search_et.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				xmlMusicResLoader.maicong_get_song_by_name(s.toString(), type, new MusicCommonCallback() {
					@Override
					public void onError(Throwable ex, boolean isOnCallBack) {
						Log.d(TAG, "请求失败:");
					}

					@Override
					public void onSuccess(String res) {
						mMusicInfoResultAbs = xmlMusicResLoader.onSuccessToDataList(type, res);
						List<MusicDataResult> musicInfoQQResultList = mMusicInfoResultAbs.getDatas();
						mSearchMusicAdapter.setSongs(musicInfoQQResultList);
					}
				});
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
	}
	
	/**
	 * 获取歌词List集合
	 * @return
	 */
	private List<LrcRow> getLrcRows(){
		return DefaultLrcParser.getIstance().getLrcRows(mContext, R.raw.hs);
	}
	
	Cancelable mCancelable;

	private void playNetWorkAddr(String url) {
		try {
			mp.reset();
			mp.setDataSource(url);
			mp.prepareAsync();
			mp.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp.start();
					handler.sendEmptyMessage(0);
				}
			});
			mp.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					handler.removeMessages(0);
				}
			});
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	Handler handler = new Handler(){
		public void handleMessage(android.os.Message msg) {
//			mPlayerSeekBar.setMax(mp.getDuration());
//			mPlayerSeekBar.setProgress(mp.getCurrentPosition());
			lrcView.seekTo(mp.getCurrentPosition(), false, false);
			handler.sendEmptyMessageDelayed(0, 100);
		};
	};
	
	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.button18) {
			search_et.setText("");
		} else if (v.getId() == R.id.test_btn) {
			search_et.setText("bywjwan");
		} else {
			Button btn = (Button) v;
			String text = (String) btn.getText();
			search_et.setText(search_et.getText().toString() + text);
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mp.release();
	}

}
