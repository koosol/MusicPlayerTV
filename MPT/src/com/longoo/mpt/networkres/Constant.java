package com.longoo.mpt.networkres;

import java.util.HashMap;

/**
 * 一些常量.
 * @author hailong.qiu
 *
 */
public class Constant {
	
	public static final String MUSIC_SELECT_TYPE_QQ = "qq";
	public static final String MUSIC_SELECT_TYPE_XIAMI = "xiami";
	public static final String MUSIC_SELECT_TYPE_BAIDU = "baidu";
	public static final String MUSIC_SELECT_TYPE_KUWO = "kuwo";
	
	/**
	 * 搜索的关键字字段.
	 * 根据类型获取关键字字段.
	 * 然后填写关键字用来搜索资源.
	 */
	public static final HashMap<String, String> SEARCH_KEY_WORDS = new HashMap<String, String>() {
		{
			put("qq", "w"); // qq音乐
			put("xiami", "key"); // 虾米
			put("5sing", "keyword"); // 5sing，migu，kugou
			put("migu", "keyword"); // 5sing，migu，kugou
			put("kugou", "keyword"); // 5sing，migu，kugou
			put("ttpod", "q"); // ttpod，soundcloud，1ting
			put("soundcloud", "q"); // ttpod，soundcloud，1ting
			put("1ting", "q"); // ttpod，soundcloud，1ting
			put("kuwo", "all"); // kuwo
			put("baidu", "word"); // baidu
			put("163", "s"); // 163
		}
	};
	
}

/*
获取电台的播放列表.
http://douban.fm/j/mine/playlist?from=mainsite&channel=1&kbps=128&type=n
http://www.douban.com/j/app/radio/channels
*/