package com.longoo.mpt.networkres;

public class MusicDataResult {

	private String radio_songid; // 歌曲ID.用于找出真实路径.
	private String song_name; // 歌曲名称
	private String singer_name; // 歌手
	
	public String getRadio_songid() {
		return radio_songid;
	}

	public void setRadio_songid(String radio_songid) {
		this.radio_songid = radio_songid;
	}

	public String getSong_name() {
		return song_name;
	}

	public void setSong_name(String songName) {
		this.song_name = songName;
	}

	public String getSinger_name() {
		return singer_name;
	}

	public void setSinger_name(String singerName) {
		this.singer_name = singerName;
	}

	@Override
	public String toString() {
		return "MusicDataResultAbs [radio_songid=" + radio_songid + ", song_name=" + song_name + ", singer_name="
				+ singer_name + "]";
	}
	
}
