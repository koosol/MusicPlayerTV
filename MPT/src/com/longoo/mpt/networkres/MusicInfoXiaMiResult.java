package com.longoo.mpt.networkres;

import java.util.ArrayList;
import java.util.List;

import com.longoo.mpt.networkres.MusicInfoQQResult.SearchDataList;

/**
 * 请求网络音乐返回的JSON数据.
 * 
 * @author hailong.qiu
 *
 */
public class MusicInfoXiaMiResult extends MusicInfoResultAbs {
	/* 虾米返回数据 */
	private String state;
	private String message;
	private String request_id;
	private MusicData data;

	private List<MusicDataResult> dataList;

	@Override
	public List<MusicDataResult> getDatas() {
		if (data != null) {
			List<SongInfo> songInfos = data.getSongs();
			if (songInfos != null) {
				dataList = new ArrayList<MusicDataResult>();
				for (SongInfo songInfo : songInfos) {
					MusicDataResult musicDataResult = new MusicDataResult();
					musicDataResult.setSong_name(songInfo.getSong_name()); // 歌名
					musicDataResult.setSinger_name(songInfo.getArtist_name()); // 歌手
					musicDataResult.setRadio_songid(songInfo.getSong_id() + ""); // 设置歌曲ID.
					dataList.add(musicDataResult);
				}
				// 设置页码
				setCurrent_page(data.getPrevious()); // 当前页码.
				setCurrent_num(songInfos.size()); // 一页的数量.
				setTotal(data.getTotal()); // 数据 总数.
			}
		}
		return dataList;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getRequest_id() {
		return request_id;
	}

	public void setRequest_id(String request_id) {
		this.request_id = request_id;
	}

	public MusicData getData() {
		return data;
	}

	public void setData(MusicData data) {
		this.data = data;
	}

	public class MusicData {
		List<SongInfo> songs;
		private int previous;
		private int next;
		private int total;

		public List<SongInfo> getSongs() {
			return songs;
		}

		public void setSongs(List<SongInfo> songs) {
			this.songs = songs;
		}

		public int getPrevious() {
			return previous;
		}

		public void setPrevious(int previous) {
			this.previous = previous;
		}

		public int getNext() {
			return next;
		}

		public void setNext(int next) {
			this.next = next;
		}

		public int getTotal() {
			return total;
		}

		public void setTotal(int total) {
			this.total = total;
		}
	}

	public class SongInfo {
		int song_id;
		String song_name;
		String album_id;
		String album_name;
		String album_logo;
		String artist_id;
		String artist_name;
		String artist_logo;
		String listen_file;
		int demo;
		String lyric;
		int is_play;
		int play_counts;
		String singer;

		public int getSong_id() {
			return song_id;
		}

		public void setSong_id(int song_id) {
			this.song_id = song_id;
		}

		public String getSong_name() {
			return song_name;
		}

		public void setSong_name(String song_name) {
			this.song_name = song_name;
		}

		public String getAlbum_id() {
			return album_id;
		}

		public void setAlbum_id(String album_id) {
			this.album_id = album_id;
		}

		public String getAlbum_name() {
			return album_name;
		}

		public void setAlbum_name(String album_name) {
			this.album_name = album_name;
		}

		public String getAlbum_logo() {
			return album_logo;
		}

		public void setAlbum_logo(String album_logo) {
			this.album_logo = album_logo;
		}

		public String getArtist_id() {
			return artist_id;
		}

		public void setArtist_id(String artist_id) {
			this.artist_id = artist_id;
		}

		public String getArtist_name() {
			return artist_name;
		}

		public void setArtist_name(String artist_name) {
			this.artist_name = artist_name;
		}

		public String getArtist_logo() {
			return artist_logo;
		}

		public void setArtist_logo(String artist_logo) {
			this.artist_logo = artist_logo;
		}

		public String getListen_file() {
			return listen_file;
		}

		public void setListen_file(String listen_file) {
			this.listen_file = listen_file;
		}

		public int getDemo() {
			return demo;
		}

		public void setDemo(int demo) {
			this.demo = demo;
		}

		public String getLyric() {
			return lyric;
		}

		public void setLyric(String lyric) {
			this.lyric = lyric;
		}

		public int getIs_play() {
			return is_play;
		}

		public void setIs_play(int is_play) {
			this.is_play = is_play;
		}

		public int getPlay_counts() {
			return play_counts;
		}

		public void setPlay_counts(int play_counts) {
			this.play_counts = play_counts;
		}

		public String getSinger() {
			return singer;
		}

		public void setSinger(String singer) {
			this.singer = singer;
		}

	}

}
