package com.longoo.mpt.networkres;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;
import org.xutils.x;
import org.xutils.common.Callback.Cancelable;
import org.xutils.common.Callback.CommonCallback;
import org.xutils.http.RequestParams;

import com.google.gson.Gson;
import com.longoo.mpt.R;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Log;

/**
 * 从XML中将音乐网络资源加载.(解析音乐资源XML)
 * 
 * @author hailong.qiu
 *
 */
public class XmlMusicResLoader {

	private static final String TAG = "XmlMusicResLoader";

	// 网络音乐资料XML属性.
	private static final String MUSIC_XML_KEY = "music";
	private static final String MUSIC_RES_XML_KEY = "res";
	// res 内部属性.
	private static final String MUSIC_RES_METHOD = "method";
	private static final String MUSIC_RES_PROXY = "proxy";
	private static final String MUSIC_RES_REFERER = "referer";
	private static final String MUSIC_RES_TYPE = "type";
	private static final String MUSIC_RES_URL = "url";
	private static final String MUSIC_RES_BODY_XML_KEY = "body";

	private Context mContext;
	private int mXmlEventType;
	private Resources mResources;
	private HashMap<String, MusicNetWorkRes> mMusicSearchMap;
	private HashMap<String, MusicNetWorkRes> mMusicSongInfoMap;

	public XmlMusicResLoader(Context context) {
		this.mContext = context;
		this.mResources = this.mContext.getResources();
		// 加载网络音乐资源XML.
		mMusicSearchMap = initLoadMusicResXml(R.xml.music_network_search_resources);
		// 加载网络音乐资源播放地址XML.
		mMusicSongInfoMap = initLoadMusicResXml(R.xml.music_network_song_info_resources);
		// Log.d(TAG, "XmlMusicResLoader search map size:" +
		// mMusicSearchMap.size() + " song info map size:"
		// + mMusicSongInfoMap.size());
	}

	public HashMap<String, MusicNetWorkRes> getMusicSearchMap() {
		return this.mMusicSearchMap;
	}

	public HashMap<String, MusicNetWorkRes> getMusicSongInfoMap() {
		return this.mMusicSongInfoMap;
	}

	public HashMap<String, MusicNetWorkRes> initLoadMusicResXml(int musicResID) {
		XmlResourceParser xrp = mContext.getResources().getXml(musicResID);
		HashMap<String, MusicNetWorkRes> musicNetWorkResMap = new HashMap<String, MusicNetWorkRes>();
		MusicNetWorkRes musicNetWorkRes = new MusicNetWorkRes();

		try {
			mXmlEventType = xrp.next();
			while (mXmlEventType != XmlResourceParser.END_DOCUMENT) {
				if (mXmlEventType == XmlResourceParser.START_TAG) {
					String attr = xrp.getName();
					if (MUSIC_XML_KEY.compareTo(attr) == 0) { // <music
						//
					} else if (MUSIC_RES_XML_KEY.compareTo(attr) == 0) { // <res
						musicNetWorkRes = new MusicNetWorkRes();
						// 获取 <res 的属性.
						String method = getString(xrp, MUSIC_RES_METHOD, null); // method
						String proxy = getString(xrp, MUSIC_RES_PROXY, "false"); // proxy
						String referer = getString(xrp, MUSIC_RES_REFERER, null); // referer
						String type = getString(xrp, MUSIC_RES_TYPE, null); // type
						String url = getString(xrp, MUSIC_RES_URL, null); // url
						//
						musicNetWorkRes.setMethod(method);
						musicNetWorkRes.setProxy(proxy);
						musicNetWorkRes.setReferer(referer);
						musicNetWorkRes.setType(type);
						musicNetWorkRes.setUrl(url);
						//
						// Log.d(TAG, "<res attribute " +
						// musicNetWorkRes.toString());
						musicNetWorkResMap.put(musicNetWorkRes.getType(), musicNetWorkRes);
					} else if (MUSIC_RES_BODY_XML_KEY.compareTo(attr) == 0) { // <body
						HashMap<String, String> body = new HashMap<String, String>();
						int count = xrp.getAttributeCount();
						// Log.d(TAG, "<body attribute count:" + count);
						for (int i = 0; i < count; i++) {
							String name = xrp.getAttributeName(i);
							String value = getString(xrp, name, null);
							body.put(name, value);
							// Log.d(TAG, "<body attribute name:" + name + "
							// value:" + value);
						}
						musicNetWorkRes.setBody(body);
						musicNetWorkResMap.put(musicNetWorkRes.getType(), musicNetWorkRes);
					}
				} else if (mXmlEventType == XmlResourceParser.END_TAG) {
				}
				mXmlEventType = xrp.next();
			}
			xrp.close();
			return musicNetWorkResMap;
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取整数
	 */
	private int getInteger(XmlResourceParser xrp, String name, int defValue) {
		int resId = xrp.getAttributeResourceValue(null, name, 0);
		String s;
		if (resId == 0) {
			s = xrp.getAttributeValue(null, name);
			if (null == s)
				return defValue;
			try {
				int ret = Integer.valueOf(s);
				return ret;
			} catch (NumberFormatException e) {
				return defValue;
			}
		} else {
			return Integer.parseInt(mContext.getResources().getString(resId));
		}
	}

	/**
	 * 获取颜色.
	 */
	private int getColor(XmlResourceParser xrp, String name, int defValue) {
		int resId = xrp.getAttributeResourceValue(null, name, 0);
		String s;
		if (resId == 0) {
			s = xrp.getAttributeValue(null, name);
			if (null == s)
				return defValue;
			try {
				int ret = Integer.valueOf(s);
				return ret;
			} catch (NumberFormatException e) {
				return defValue;
			}
		} else {
			return mContext.getResources().getColor(resId);
		}
	}

	/**
	 * 获取字符串
	 */
	private String getString(XmlResourceParser xrp, String name, String defValue) {
		int resId = xrp.getAttributeResourceValue(null, name, 0);
		if (resId == 0) {
			return xrp.getAttributeValue(null, name);
		} else {
			return mContext.getResources().getString(resId);
		}
	}

	/**
	 * 获取浮点型
	 */
	private float getFloat(XmlResourceParser xrp, String name, float defValue) {
		int resId = xrp.getAttributeResourceValue(null, name, 0);
		if (resId == 0) {
			String s = xrp.getAttributeValue(null, name);
			if (null == s)
				return defValue;
			try {
				float ret;
				if (s.endsWith("%p")) {
					ret = Float.parseFloat(s.substring(0, s.length() - 2)) / 100;
				} else {
					ret = Float.parseFloat(s);
				}
				return ret;
			} catch (NumberFormatException e) {
				return defValue;
			}
		} else {
			return mContext.getResources().getDimension(resId);
		}
	}

	/*
	 * 获取布尔值.
	 */
	private boolean getBoolean(XmlResourceParser xrp, String name, boolean defValue) {
		String s = xrp.getAttributeValue(null, name);
		if (null == s)
			return defValue;
		try {
			boolean ret = Boolean.parseBoolean(s);
			return ret;
		} catch (NumberFormatException e) {
			return defValue;
		}
	}

	/**
	 * 获取图片资源
	 */
	private Drawable getDrawable(XmlResourceParser xrp, String name, Drawable defValue) {
		int resId = xrp.getAttributeResourceValue(null, name, 0);
		if (0 == resId)
			return defValue;
		return mResources.getDrawable(resId);
	}

	/**
	 * 关键词搜索.
	 * 
	 * @param query
	 *            关键字
	 * @param site
	 *            类型.
	 */
	public void maicong_get_song_by_name(final String query, final String site, final MusicCommonCallback cb) {
		if (TextUtils.isEmpty(query)) {
			return;
		}
		MusicNetWorkRes radioSearchUrl = maicong_song_urls(query, "query", site);
		if ((null == radioSearchUrl)) {
			return;
		}
		// 请求音乐网络地址，获取数据.
		maicong_curl(radioSearchUrl, new CommonCallback<String>() {

			@Override
			public void onCancelled(CancelledException canEx) {
				cb.onCancelled(canEx);
			}

			@Override
			public void onError(Throwable ex, boolean isOnCallback) {
				cb.onError(ex, isOnCallback);
			}

			@Override
			public void onFinished() {
				cb.onFinished();
			}

			@Override
			public void onSuccess(String res) {
				cb.onSuccess(res);
			}
		});
	}

	public MusicNetWorkRes maicong_song_urls(String value, String type, String site) {
		if (TextUtils.isEmpty(value)) {
			return null;
		}
		String query = ("query".equals(type) ? value : "");
		String songid = ("songid".equals(type) ? value : "");
		MusicNetWorkRes radioSearchUrls = null;
		MusicNetWorkRes newRadioSearchUrls = null;

		if ("query".equals(type)) { // 搜索.
			// 获取音乐网络请求格式.
			radioSearchUrls = mMusicSearchMap.get(site);
		} else if ("songid".equals(type)) {
			radioSearchUrls = mMusicSongInfoMap.get(site);
		}
		// Log.d(TAG, "maicong_song_urls radioSearchUrls:" + radioSearchUrls);
		if (radioSearchUrls != null) {
			newRadioSearchUrls = new MusicNetWorkRes();
			// referer addr 添加搜索关键字.
			String referer = radioSearchUrls.getReferer();
			String url = radioSearchUrls.getUrl();
			referer = String.format(referer, value);
			url = String.format(url, value);
			newRadioSearchUrls.setReferer(referer);
			newRadioSearchUrls.setUrl(url);
			// keyword 设置搜索关键字.
			HashMap<String, String> body = radioSearchUrls.getBody();
			if (body != null) {
				body.put(Constant.SEARCH_KEY_WORDS.get(site), value);
				if ("songid".equals(type)) {
					if (Constant.MUSIC_SELECT_TYPE_QQ.equals(site)) { // QQ
						body.put("midlist", value); // 填入 song id.
					} else if (Constant.MUSIC_SELECT_TYPE_BAIDU.equals(site)) { // 百度.
						body.put("songid", value); // 填入 song id.
					} else if (Constant.MUSIC_SELECT_TYPE_KUWO.equals(site)) { // 酷我.
						body.put("rid", "MUSIC_" + value); // 添加 song id.
					}
				}
				newRadioSearchUrls.setBody(body);
			}
			// 复制属性.
			newRadioSearchUrls.setMethod(radioSearchUrls.getMethod());
			newRadioSearchUrls.setType(radioSearchUrls.getType());
			newRadioSearchUrls.setProxy(radioSearchUrls.getProxy());
		}
		return newRadioSearchUrls;
	}

	Cancelable mCancelable;
	Cancelable mLrcGetCancelable;
	
	public void maicong_curl(MusicNetWorkRes radioSearchUrl, CommonCallback cb) {
		MusicNetWorkRes musicNetWorkRes = radioSearchUrl;

		String type = musicNetWorkRes.getType();
		String method = musicNetWorkRes.getMethod();
		HashMap<String, String> body = musicNetWorkRes.getBody();
		String referer = musicNetWorkRes.getReferer();
		String url = musicNetWorkRes.getUrl();
		String proxy = musicNetWorkRes.getProxy();
		// 填写请求数据.
		RequestParams params = new RequestParams(url);
		params.addHeader("referer", referer);
		params.addHeader("proxy", proxy);
		if (body != null) {
			for (String key : body.keySet()) {
				String value = body.get(key);
				params.addBodyParameter(key, value);
			}
		}
		// 取消下载.
		if (mCancelable != null) {
			if (!mCancelable.isCancelled())
				mCancelable.cancel();
		}
		// 请求网络.
		if (method.equals("GET")) { // get请求.
			mCancelable = x.http().get(params, cb);
		}
	}

	public void maicong_get_song_by_id(final String songid, final String site, final MusicCommonCallback cb) {
		if (TextUtils.isEmpty(songid) || TextUtils.isEmpty(site)) {
			return;
		}
		MusicNetWorkRes radioSearchUrl = maicong_song_urls(songid, "songid", site);
		if (null == radioSearchUrl)
			return;
		maicong_curl(radioSearchUrl, new CommonCallback<String>() {

			@Override
			public void onCancelled(CancelledException canEx) {
				cb.onCancelled(canEx);
			}

			@Override
			public void onError(Throwable ex, boolean isOnCallback) {
				cb.onError(ex, isOnCallback);
			}

			@Override
			public void onFinished() {
				cb.onFinished();
			}

			@Override
			public void onSuccess(String res) {
				if (TextUtils.isEmpty(res))
					return;
				String name = "";
				String author = "";
				String music = "";
				String pic = "";
				String newSongid = "";
				MusicSongInfo musicSongInfo = new MusicSongInfo();
				if (Constant.MUSIC_SELECT_TYPE_QQ.equals(site)) { // QQ
					JSONObject jb;
					try {
						jb = new JSONObject(res);
						JSONArray jaData = jb.getJSONArray("data");
						JSONObject jbDataOne = jaData.getJSONObject(0);
						// 获取歌曲ID，歌曲名称.
						newSongid = jbDataOne.getString("songid");
						name = jbDataOne.getString("songname");
						String albummid = jbDataOne.getString("albummid");
						// 获取封面ID.
						String radio_pic = albummid.substring(albummid.length() - 2, albummid.length() - 1);
						radio_pic += "/" + albummid.substring(albummid.length() - 1);
						radio_pic += "/" + albummid;
						// 获取播放地址.
						JSONObject jbUrl = jb.getJSONObject("url");
						music = jbUrl.getString(newSongid);
						// 获取歌手.
						JSONArray jasinger = jbDataOne.getJSONArray("singer");
						JSONObject jasingerOne = jasinger.getJSONObject(0);
						author = jasingerOne.getString("name");
						// 设置封面
						pic = "http://imgcache.qq.com/music/photo/mid_album_300/" + radio_pic + ".jpg";
					} catch (JSONException e) {
						e.printStackTrace();
					}
				} else if (Constant.MUSIC_SELECT_TYPE_XIAMI.equals(site)) { // 虾米
					try {
						JSONObject jb = new JSONObject(res);
						JSONObject jbData = jb.getJSONObject("data");
						JSONArray jaTrackList = jbData.getJSONArray("trackList");
						JSONObject jb3 = jaTrackList.getJSONObject(0);
						music = maicong_decode_xiami_location(jb3.getString("location")); // 播放地址
						pic = jb3.getString("album_pic"); // 封面.
						newSongid = jb3.getString("song_id"); // 歌曲ID.
						name = jb3.getString("title"); // 歌曲名称.
						author = jb3.getString("artist"); // 歌手.
					} catch (JSONException e) {
						e.printStackTrace();
					}
				} else if (Constant.MUSIC_SELECT_TYPE_BAIDU.equals(site)) { // 百度.
					try {
						JSONObject jb = new JSONObject(res);
						JSONObject jbsonginfo = jb.getJSONObject("songinfo");
						JSONObject jbbitrate = jb.getJSONObject("bitrate");
						newSongid = jbsonginfo.getString("song_id");
						name = jbsonginfo.getString("title");
						author = jbsonginfo.getString("author");
						music = jbbitrate.getString("file_link");
						pic = jbsonginfo.getString("pic_big");
					} catch (JSONException e) {
						e.printStackTrace();
					}
				} else if (Constant.MUSIC_SELECT_TYPE_KUWO.equals(site)) { // 酷我.
					// 获取歌曲ID.
					int musicIdStart = res.indexOf("<music_id>");
					int musicIdEnd = res.indexOf("</music_id>");
					String music_id = res.substring(musicIdStart + "<music_id>".length(), musicIdEnd);
					if (!TextUtils.isEmpty(music_id)) {
						newSongid = music_id;
						// 获取歌曲名称.
						int nameStart = res.indexOf("<name>");
						int nameEnd = res.indexOf("</name>");
						name = res.substring(nameStart + "<name>".length(), nameEnd);
						// 获取歌曲歌手.
						int singerStart = res.indexOf("<singer>");
						int singerEnd = res.indexOf("</singer>");
						author = res.substring(singerStart + "<singer>".length(), singerEnd);
						// 获取真实播放地址.
						int mp3dlStart = res.indexOf("<mp3dl>");
						int mp3dlEnd = res.indexOf("</mp3dl>");
						String mp3dl = res.substring(mp3dlStart + "<mp3dl>".length(), mp3dlEnd);
						int mp3pathStart = res.indexOf("<mp3path>");
						int mp3pathEnd = res.indexOf("</mp3path>");
						String mp3path = res.substring(mp3pathStart + "<mp3path>".length(), mp3pathEnd);
						music = "http://" + mp3dl + "/resource/" + mp3path;
						// 获取封面. artist_pic
						int artist_picStart = res.indexOf("<artist_pic>");
						int artist_picEnd = res.indexOf("</artist_pic>");
						pic = res.substring(artist_picStart + "<artist_pic>".length(), artist_picEnd);
					} else {
						return;
					}
				}
				Log.d(TAG, "maicong_get_song_by_id onSuccess res:" + res);
				musicSongInfo.setType(site);
				musicSongInfo.setSongid(newSongid);
				musicSongInfo.setName(name);
				musicSongInfo.setAuthor(author);
				musicSongInfo.setMusic(music);
				musicSongInfo.setPic(pic);
				cb.onSuccess(musicSongInfo);
			}
		});
	}

	public MusicInfoResultAbs onSuccessToDataList(String type, String res) {
		MusicInfoResultAbs musicInfoResultAbs = null;
		if (TextUtils.isEmpty(type) || TextUtils.isEmpty(res))
			return musicInfoResultAbs;
		Gson gson = new Gson();
		if (Constant.MUSIC_SELECT_TYPE_QQ.equals(type)) { // QQ
			MusicInfoQQResult musicInfoQQResult = gson.fromJson(res, MusicInfoQQResult.class);
			musicInfoResultAbs = musicInfoQQResult;
		} else if (Constant.MUSIC_SELECT_TYPE_XIAMI.equals(type)) { // 虾米
			MusicInfoXiaMiResult musicInfoXaiMiResult = gson.fromJson(res, MusicInfoXiaMiResult.class);
			musicInfoResultAbs = musicInfoXaiMiResult;
		} else if (Constant.MUSIC_SELECT_TYPE_BAIDU.equals(type)) { // 百度
			MusicInfoBaiduResult musicInfoBaiduResult = gson.fromJson(res, MusicInfoBaiduResult.class);
			musicInfoResultAbs = musicInfoBaiduResult;
		} else if (Constant.MUSIC_SELECT_TYPE_KUWO.equals(type)) { // 酷我
			MusicInfoKuWoResult musicInfoKuWoResult = gson.fromJson(res, MusicInfoKuWoResult.class);
			musicInfoResultAbs = musicInfoKuWoResult;
//			Log.d(TAG, "onSuccessToDataList musicInfoKuWoResult:" + musicInfoKuWoResult);
		}
		return musicInfoResultAbs;
	}

	/**
	 * 解密虾米 播放 location.
	 */
	public String maicong_decode_xiami_location(String location) {
		if (TextUtils.isEmpty(location))
			return null;
		String url = "";
		location = location.trim(); // 去掉前后的空格.
		Integer line = Integer.parseInt(location.charAt(0) + "");
		int locLen = location.length();
		int rows = (locLen - 1) / line;
		int extra = (locLen - 1) % line;
		location = location.substring(1);
		//
		List<String> resultList = new ArrayList<String>();
		//
		for (int i = 0; i < extra; ++i) {
			int start = (rows + 1) * i;
			int end = (rows + 1) * (i + 1);
			String result = location.substring(start, end);
			resultList.add(result);
		}
		//
		for (int i = 0; i < (line - extra); i++) {
			int start = (rows + 1) * extra + (rows * i);
			int end = (rows + 1) * extra + (rows * i) + rows;
			String result = location.substring(start, end);
			resultList.add(result);
		}
		for (int i = 0; i < (rows + 1); i++) {
			for (int j = 0; j < line; j++) {
				if (j >= resultList.size() || i >= resultList.get(j).length()) {
					continue;
				}
				url += resultList.get(j).charAt(i);
			}
		}
		url = URLDecoder.decode(url);
		url = url.replace("^", "0");
		return url;
	}

	/**
	 * 获取在线歌词.
	 * 
	 * @param name
	 *            歌曲名称.
	 * @param author
	 *            歌手.
	 */
	public void getOnlineLrc(final String name, final String author, final MusicCommonCallback cb) {
		String newName = "";
		String newAuthor = "";
		try {
			newName = URLEncoder.encode(name,   "utf-8");
			if (!TextUtils.isEmpty(author))
				newAuthor = URLEncoder.encode(author,   "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} 
		String musicAddress = "http://geci.me/api/lyric/" + newName + "/" + newAuthor;
		if (mLrcGetCancelable != null) {
			if (!mLrcGetCancelable.isCancelled())
				mLrcGetCancelable.cancel();
		}
		RequestParams params = new RequestParams(musicAddress);
		mLrcGetCancelable = x.http().get(params, new CommonCallback<String>() {
			@Override
			public void onCancelled(CancelledException canEx) {
				cb.onCancelled(canEx);
			}

			@Override
			public void onError(Throwable ex, boolean isOnCallBack) {
				cb.onError(ex, isOnCallBack);
			}

			@Override
			public void onFinished() {
				cb.onFinished();
			}

			@Override
			public void onSuccess(String res) {
				cb.onSuccess(res);
			}
		});
	}
}
