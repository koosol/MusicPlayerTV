package com.longoo.mpt.networkres;

import java.util.ArrayList;
import java.util.List;

import com.longoo.mpt.networkres.MusicInfoBaiduResult.Songs;

/**
 * 酷我搜索返回数据.
 */
public class MusicInfoKuWoResult extends MusicInfoResultAbs {

	String RN; // 一页搜索的数量.
	String PN; // 页码.
	String HIT;
	String TOTAL; // 数据总量
	String SHOW;
	String NEW;
	String MSHOW;
	String HITMODE;
	String ARTISTPIC;
	String HIT_BUT_OFFLINE;
	List<Abslists> abslist;
	List<MusicDataResult> dataList;
	
	@Override
	public List<MusicDataResult> getDatas() {
		if (abslist != null) {
			dataList = new ArrayList<MusicDataResult>();
			// 将数据转换出来，并且获取歌曲ID.
			for (Abslists songInfo : abslist) {
				MusicDataResult musicDataResult = new MusicDataResult();
				musicDataResult.setSong_name(songInfo.getSONGNAME()); // 歌曲名称.
				musicDataResult.setSinger_name(songInfo.getARTIST()); // 歌手.
				musicDataResult.setRadio_songid(songInfo.getMUSICRID().replace("MUSIC_", "")); // 设置歌曲ID.
				//
				dataList.add(musicDataResult);
			}
			// 设置页码，数量，总数.
			setCurrent_page(Integer.parseInt(PN)); // 当前页码.
			setCurrent_num(abslist.size()); // 一页的数量.
			setTotal(Integer.parseInt(TOTAL)); // 数据 总数.
		}
		return dataList;
	}

	public String getRN() {
		return RN;
	}

	public void setRN(String rN) {
		RN = rN;
	}

	public String getPN() {
		return PN;
	}

	public void setPN(String pN) {
		PN = pN;
	}

	public String getHIT() {
		return HIT;
	}

	public void setHIT(String hIT) {
		HIT = hIT;
	}

	public String getTOTAL() {
		return TOTAL;
	}

	public void setTOTAL(String tOTAL) {
		TOTAL = tOTAL;
	}

	public String getSHOW() {
		return SHOW;
	}

	public void setSHOW(String sHOW) {
		SHOW = sHOW;
	}

	public String getNEW() {
		return NEW;
	}

	public void setNEW(String nEW) {
		NEW = nEW;
	}

	public String getMSHOW() {
		return MSHOW;
	}

	public void setMSHOW(String mSHOW) {
		MSHOW = mSHOW;
	}

	public String getHITMODE() {
		return HITMODE;
	}

	public void setHITMODE(String hITMODE) {
		HITMODE = hITMODE;
	}

	public String getARTISTPIC() {
		return ARTISTPIC;
	}

	public void setARTISTPIC(String aRTISTPIC) {
		ARTISTPIC = aRTISTPIC;
	}

	public String getHIT_BUT_OFFLINE() {
		return HIT_BUT_OFFLINE;
	}

	public void setHIT_BUT_OFFLINE(String hIT_BUT_OFFLINE) {
		HIT_BUT_OFFLINE = hIT_BUT_OFFLINE;
	}

	public List<Abslists> getAbslist() {
		return abslist;
	}

	public void setAbslist(List<Abslists> abslist) {
		this.abslist = abslist;
	}

	@Override
	public String toString() {
		return "MusicInfoKuWoResult [RN=" + RN + ", PN=" + PN + ", HIT=" + HIT + ", TOTAL=" + TOTAL + ", SHOW=" + SHOW
				+ ", NEW=" + NEW + ", MSHOW=" + MSHOW + ", HITMODE=" + HITMODE + ", ARTISTPIC=" + ARTISTPIC
				+ ", HIT_BUT_OFFLINE=" + HIT_BUT_OFFLINE + ", abslist=" + abslist + "]";
	}

	public class Abslists {
		String MUSICRID;
		String SONGNAME; // 歌曲名称
		String ARTIST; // 歌手
		String ARTISTID;
		String ALBUM;
		String ALBUMID;
		String FORMATS;
		String SCORE100;
		String NSIG1;
		String NSIG2;
		String MP3NSIG1;
		String MP3NSIG2;
		String MP3RID;
		String MKVNSIG1;
		String MKVNSIG2;
		String MKVRID;
		String HASECHO;
		String NEW;
		String MVPIC;
		String UPLOADER;
		String UPTIME;
		String IS_POINT;
		String MUTI_VER;
		String ONLINE;
		String IS_EXT;
		String PAY;
		String COPYRIGHT;

		public String getMUSICRID() {
			return MUSICRID;
		}

		public void setMUSICRID(String mUSICRID) {
			MUSICRID = mUSICRID;
		}

		public String getSONGNAME() {
			return SONGNAME;
		}

		public void setSONGNAME(String sONGNAME) {
			SONGNAME = sONGNAME;
		}

		public String getARTIST() {
			return ARTIST;
		}

		public void setARTIST(String aRTIST) {
			ARTIST = aRTIST;
		}

		public String getARTISTID() {
			return ARTISTID;
		}

		public void setARTISTID(String aRTISTID) {
			ARTISTID = aRTISTID;
		}

		public String getALBUM() {
			return ALBUM;
		}

		public void setALBUM(String aLBUM) {
			ALBUM = aLBUM;
		}

		public String getALBUMID() {
			return ALBUMID;
		}

		public void setALBUMID(String aLBUMID) {
			ALBUMID = aLBUMID;
		}

		public String getFORMATS() {
			return FORMATS;
		}

		public void setFORMATS(String fORMATS) {
			FORMATS = fORMATS;
		}

		public String getSCORE100() {
			return SCORE100;
		}

		public void setSCORE100(String sCORE100) {
			SCORE100 = sCORE100;
		}

		public String getNSIG1() {
			return NSIG1;
		}

		public void setNSIG1(String nSIG1) {
			NSIG1 = nSIG1;
		}

		public String getNSIG2() {
			return NSIG2;
		}

		public void setNSIG2(String nSIG2) {
			NSIG2 = nSIG2;
		}

		public String getMP3NSIG1() {
			return MP3NSIG1;
		}

		public void setMP3NSIG1(String mP3NSIG1) {
			MP3NSIG1 = mP3NSIG1;
		}

		public String getMP3NSIG2() {
			return MP3NSIG2;
		}

		public void setMP3NSIG2(String mP3NSIG2) {
			MP3NSIG2 = mP3NSIG2;
		}

		public String getMP3RID() {
			return MP3RID;
		}

		public void setMP3RID(String mP3RID) {
			MP3RID = mP3RID;
		}

		public String getMKVNSIG1() {
			return MKVNSIG1;
		}

		public void setMKVNSIG1(String mKVNSIG1) {
			MKVNSIG1 = mKVNSIG1;
		}

		public String getMKVNSIG2() {
			return MKVNSIG2;
		}

		public void setMKVNSIG2(String mKVNSIG2) {
			MKVNSIG2 = mKVNSIG2;
		}

		public String getMKVRID() {
			return MKVRID;
		}

		public void setMKVRID(String mKVRID) {
			MKVRID = mKVRID;
		}

		public String getHASECHO() {
			return HASECHO;
		}

		public void setHASECHO(String hASECHO) {
			HASECHO = hASECHO;
		}

		public String getNEW() {
			return NEW;
		}

		public void setNEW(String nEW) {
			NEW = nEW;
		}

		public String getMVPIC() {
			return MVPIC;
		}

		public void setMVPIC(String mVPIC) {
			MVPIC = mVPIC;
		}

		public String getUPLOADER() {
			return UPLOADER;
		}

		public void setUPLOADER(String uPLOADER) {
			UPLOADER = uPLOADER;
		}

		public String getUPTIME() {
			return UPTIME;
		}

		public void setUPTIME(String uPTIME) {
			UPTIME = uPTIME;
		}

		public String getIS_POINT() {
			return IS_POINT;
		}

		public void setIS_POINT(String iS_POINT) {
			IS_POINT = iS_POINT;
		}

		public String getMUTI_VER() {
			return MUTI_VER;
		}

		public void setMUTI_VER(String mUTI_VER) {
			MUTI_VER = mUTI_VER;
		}

		public String getONLINE() {
			return ONLINE;
		}

		public void setONLINE(String oNLINE) {
			ONLINE = oNLINE;
		}

		public String getIS_EXT() {
			return IS_EXT;
		}

		public void setIS_EXT(String iS_EXT) {
			IS_EXT = iS_EXT;
		}

		public String getPAY() {
			return PAY;
		}

		public void setPAY(String pAY) {
			PAY = pAY;
		}

		public String getCOPYRIGHT() {
			return COPYRIGHT;
		}

		public void setCOPYRIGHT(String cOPYRIGHT) {
			COPYRIGHT = cOPYRIGHT;
		}

		@Override
		public String toString() {
			return "Abslists [MUSICRID=" + MUSICRID + ", SONGNAME=" + SONGNAME + ", ARTIST=" + ARTIST + ", ARTISTID="
					+ ARTISTID + ", ALBUM=" + ALBUM + ", ALBUMID=" + ALBUMID + ", FORMATS=" + FORMATS + ", SCORE100="
					+ SCORE100 + ", NSIG1=" + NSIG1 + ", NSIG2=" + NSIG2 + ", MP3NSIG1=" + MP3NSIG1 + ", MP3NSIG2="
					+ MP3NSIG2 + ", MP3RID=" + MP3RID + ", MKVNSIG1=" + MKVNSIG1 + ", MKVNSIG2=" + MKVNSIG2
					+ ", MKVRID=" + MKVRID + ", HASECHO=" + HASECHO + ", NEW=" + NEW + ", MVPIC=" + MVPIC
					+ ", UPLOADER=" + UPLOADER + ", UPTIME=" + UPTIME + ", IS_POINT=" + IS_POINT + ", MUTI_VER="
					+ MUTI_VER + ", ONLINE=" + ONLINE + ", IS_EXT=" + IS_EXT + ", PAY=" + PAY + ", COPYRIGHT="
					+ COPYRIGHT + "]";
		}

	}
}
