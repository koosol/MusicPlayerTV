package com.longoo.mpt.networkres;

import java.util.List;

/**
 * ��ȡ���߸��.
 */
public class OnlineLrcResult {
	public int count;
	public int code;
	public List<LrcInfo> result;

	public void setCount(int count) {
		this.count = count;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public void setResult(List<LrcInfo> result) {
		this.result = result;
	}

	public int getCount() {
		return count;
	}

	public int getCode() {
		return code;
	}

	public List<LrcInfo> getResult() {
		return result;
	}

	public class LrcInfo {
		int aid;
		String artist_id;
		String song;
		String lrc;
		String sid;

		public int getAid() {
			return aid;
		}

		public void setAid(int aid) {
			this.aid = aid;
		}

		public String getArtist_id() {
			return artist_id;
		}

		public void setArtist_id(String artist_id) {
			this.artist_id = artist_id;
		}

		public String getSong() {
			return song;
		}

		public void setSong(String song) {
			this.song = song;
		}

		public String getLrc() {
			return lrc;
		}

		public void setLrc(String lrc) {
			this.lrc = lrc;
		}

		public String getSid() {
			return sid;
		}

		public void setSid(String sid) {
			this.sid = sid;
		}

		@Override
		public String toString() {
			return "LrcInfo [aid=" + aid + ", artist_id=" + artist_id + ", song=" + song + ", lrc=" + lrc + ", sid="
					+ sid + "]";
		}

	}

	@Override
	public String toString() {
		return "OnlineLrcResult [count=" + count + ", code=" + code + ", result=" + result + "]";
	}

}
