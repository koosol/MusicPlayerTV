package com.longoo.mpt.networkres;

import java.util.List;

public abstract class MusicInfoResultAbs {
	
	private int current_page; // 当前页面. curpage
	private int current_num; // 一页的数据 总数
	private int total; // 数据 总数.

	public int getCurrent_page() {
		return current_page;
	}

	public void setCurrent_page(int current_page) {
		this.current_page = current_page;
	}

	public int getCurrent_num() {
		return current_num;
	}

	public void setCurrent_num(int current_num) {
		this.current_num = current_num;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}
	
	public abstract List<MusicDataResult> getDatas();
}
