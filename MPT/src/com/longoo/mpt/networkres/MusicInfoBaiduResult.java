package com.longoo.mpt.networkres;

import java.util.ArrayList;
import java.util.List;

/**
 * 百度返回的数据解析.
 * 
 * @author hailong.qiu
 *
 */
public class MusicInfoBaiduResult extends MusicInfoResultAbs {
	Data data;
	List<MusicDataResult> dataList;
	
	@Override
	public List<MusicDataResult> getDatas() {
		if (data != null) {
			dataList = new ArrayList<MusicDataResult>();
			List<Songs> songInfos = data.getSong();
			if (songInfos != null) {
				// 将数据转换出来，并且获取歌曲ID.
				for (Songs songInfo : songInfos) {
					MusicDataResult musicDataResult = new MusicDataResult();
					musicDataResult.setSong_name(songInfo.getSongname()); // 歌曲名称.
					musicDataResult.setSinger_name(songInfo.getArtistname()); // 歌手.
					musicDataResult.setRadio_songid(songInfo.getSongid()); // 设置歌曲ID.
					//
					dataList.add(musicDataResult);
				}
				// 设置页码，数量，总数.
				setCurrent_page(-1); // 当前页码.
				setCurrent_num(songInfos.size()); // 一页的数量.
				setTotal(-1); // 数据 总数.
			}
		}
		return dataList;
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public List<String> getPro() {
		return Pro;
	}

	public void setPro(List<String> pro) {
		Pro = pro;
	}

	@Override
	public String toString() {
		return "MusicInfoBaiduResult [data=" + data + ", Pro=" + Pro + "]";
	}

	public class Data {
		List<Songs> song;
		List<Artists> artist;

		public List<Songs> getSong() {
			return song;
		}

		public void setSong(List<Songs> song) {
			this.song = song;
		}

		@Override
		public String toString() {
			return "Data [song=" + song + ", artist=" + artist + "]";
		}

	}

	public class Artists {
		String artistid;
		String artistname;
		String artistpic;
		String yyr_artist;

		public String getArtistid() {
			return artistid;
		}

		public void setArtistid(String artistid) {
			this.artistid = artistid;
		}

		public String getArtistname() {
			return artistname;
		}

		public void setArtistname(String artistname) {
			this.artistname = artistname;
		}

		public String getArtistpic() {
			return artistpic;
		}

		public void setArtistpic(String artistpic) {
			this.artistpic = artistpic;
		}

		public String getYyr_artist() {
			return yyr_artist;
		}

		public void setYyr_artist(String yyr_artist) {
			this.yyr_artist = yyr_artist;
		}

		@Override
		public String toString() {
			return "Artists [artistid=" + artistid + ", artistname=" + artistname + ", artistpic=" + artistpic
					+ ", yyr_artist=" + yyr_artist + "]";
		}

	}

	public class Songs {
		String songid;
		String songname;
		String encrypted_songid;
		String bitrate_fee;
		String has_mv;
		String yyr_artist;
		String artistname;

		public String getSongid() {
			return songid;
		}

		public void setSongid(String songid) {
			this.songid = songid;
		}

		public String getSongname() {
			return songname;
		}

		public void setSongname(String songname) {
			this.songname = songname;
		}

		public String getEncrypted_songid() {
			return encrypted_songid;
		}

		public void setEncrypted_songid(String encrypted_songid) {
			this.encrypted_songid = encrypted_songid;
		}

		public String getBitrate_fee() {
			return bitrate_fee;
		}

		public void setBitrate_fee(String bitrate_fee) {
			this.bitrate_fee = bitrate_fee;
		}

		public String getHas_mv() {
			return has_mv;
		}

		public void setHas_mv(String has_mv) {
			this.has_mv = has_mv;
		}

		public String getYyr_artist() {
			return yyr_artist;
		}

		public void setYyr_artist(String yyr_artist) {
			this.yyr_artist = yyr_artist;
		}

		public String getArtistname() {
			return artistname;
		}

		public void setArtistname(String artistname) {
			this.artistname = artistname;
		}

		@Override
		public String toString() {
			return "SongS [songid=" + songid + ", songname=" + songname + ", encrypted_songid=" + encrypted_songid
					+ ", bitrate_fee=" + bitrate_fee + ", has_mv=" + has_mv + ", yyr_artist=" + yyr_artist
					+ ", artistname=" + artistname + "]";
		}

	}

	List<String> Pro = new ArrayList<String>() {
		{
			add("artist");
			add("song");
			add("album");
		}
	};

}
