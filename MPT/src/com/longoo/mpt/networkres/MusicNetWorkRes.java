package com.longoo.mpt.networkres;

import java.util.HashMap;

/**
 * 读取XML中的音乐网络资源保存起来.
 * 
 * @author hailong.qiu
 *
 */
public class MusicNetWorkRes {
	private String type;
	private String method;
	private String url;
	private String referer;
	private String proxy;

	private HashMap<String, String> body;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getReferer() {
		return referer;
	}

	public void setReferer(String referer) {
		this.referer = referer;
	}

	public String getProxy() {
		return proxy;
	}

	public void setProxy(String proxy) {
		this.proxy = proxy;
	}

	public HashMap<String, String> getBody() {
		return body;
	}

	public void setBody(HashMap<String, String> body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return "MusicNetWorkRes [type=" + type + ", method=" + method + ", url=" + url + ", referer=" + referer
				+ ", proxy=" + proxy + ", body=" + body + "]";
	}
}
