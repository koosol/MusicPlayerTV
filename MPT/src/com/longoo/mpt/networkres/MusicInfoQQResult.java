package com.longoo.mpt.networkres;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;

public class MusicInfoQQResult extends MusicInfoResultAbs {
	private static final String TAG = "XmlMusicResLoader";

	/* QQ音乐返回数据 */
	int curnum;
	int curpage;
	int perpage;
	int retcode;
	int totalnum;
	String keyword;
	List<SearchDataList> list;
	List<MusicDataResult> dataList;
	
	@Override
	public List<MusicDataResult> getDatas() {
		if (list != null) {
			dataList = new ArrayList<MusicDataResult>();
			// 将数据转换出来，并且获取歌曲ID.
			for (SearchDataList searchDataInfo : list) {
				MusicDataResult musicDataResult = new MusicDataResult();
				musicDataResult.setSong_name(searchDataInfo.getSongname()); // 歌曲名称.
				musicDataResult.setSinger_name(searchDataInfo.getSingername()); // 歌手.
				// 设置歌曲ID.
				String[] radioHash = searchDataInfo.getF().split("\\|");
				if (radioHash != null) {
					String radioSongId = radioHash[20];
					musicDataResult.setRadio_songid(radioSongId);
				}
				//
				dataList.add(musicDataResult);
			}
			// 设置页码
			setCurrent_page(curpage); // 当前页码.
			setCurrent_num(list.size()); // 一页的数量.
			setTotal(totalnum); // 数据 总数.
		}
		return dataList;
	}

	public int getCurnum() {
		return curnum;
	}

	public void setCurnum(int curnum) {
		this.curnum = curnum;
	}

	public int getCurpage() {
		return curpage;
	}

	public void setCurpage(int curpage) {
		this.curpage = curpage;
	}

	public int getPerpage() {
		return perpage;
	}

	public void setPerpage(int perpage) {
		this.perpage = perpage;
	}

	public int getRetcode() {
		return retcode;
	}

	public void setRetcode(int retcode) {
		this.retcode = retcode;
	}

	public int getTotalnum() {
		return totalnum;
	}

	public void setTotalnum(int totalnum) {
		this.totalnum = totalnum;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public List<SearchDataList> getList() {
		return list;
	}

	public void setList(List<SearchDataList> list) {
		this.list = list;
	}

	public class SearchDataList {
		String albumname;
		String downUrl;
		String f;
		int id;
		String m4a;
		int ring;
		String singername;
		String songname;
		int t;

		public String getAlbumname() {
			return albumname;
		}

		public void setAlbumname(String albumname) {
			this.albumname = albumname;
		}

		public String getDownUrl() {
			return downUrl;
		}

		public void setDownUrl(String downUrl) {
			this.downUrl = downUrl;
		}

		public String getF() {
			return f;
		}

		public void setF(String f) {
			this.f = f;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getM4a() {
			return m4a;
		}

		public void setM4a(String m4a) {
			this.m4a = m4a;
		}

		public int getRing() {
			return ring;
		}

		public void setRing(int ring) {
			this.ring = ring;
		}

		public String getSingername() {
			return singername;
		}

		public void setSingername(String singerName) {
			this.singername = singerName;
		}

		public String getSongname() {
			return songname;
		}

		public void setSongname(String songName) {
			Log.d(TAG, "setSongname:" + songName);
			this.songname = songName;
		}

		public int getT() {
			return t;
		}

		public void setT(int t) {
			this.t = t;
		}

		@Override
		public String toString() {
			return "SearchDataList [albumname=" + albumname + ", downUrl=" + downUrl + ", f=" + f + ", id=" + id
					+ ", m4a=" + m4a + ", ring=" + ring + ", singername=" + singername + ", songname=" + songname
					+ ", t=" + t + "]";
		}
	}

	@Override
	public String toString() {
		return "MusicInfoQQResult [curnum=" + curnum + ", curpage=" + curpage + ", keyword=" + keyword + ", list="
				+ list + "]";
	}

}
